<?php

/**
 * Implements hook_menu().
 */
function mass_spam_menu() {
  $items['admin/settings/mass-spam'] = array(
    'title' => t('Mass spam'),
    'description' => t('Configure the mass spam variables.'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mass_spam_settings'),
    'access arguments' => array('administer site configuration'),
  );
  
  return $items;
}

/**
 * Implements hook_comment().
 */
function mass_spam_comment(&$a1, $op) {
  if ($op == 'insert') {
    $nid = $a1['nid'];
    global $user;

    // check in user is new
    if ((time() - $user->created) < variable_get('mass_spam_new_user', 24) * 3600) {
      // check only last comments 
      $res = db_query_range("SELECT cid, timestamp FROM {comments} WHERE uid = %d ORDER BY timestamp DESC", $a1['uid'], 0, variable_get('mass_spam_comment_limit', 10));
      $time = time() - variable_get('mass_spam_post_period', 30) * 60;
      $count = 0;
      while ($ob = db_fetch_object($res)) {
        if ($ob->timestamp > $time) {
          $count++;
        }
      }

      if ($count >= variable_get('mass_spam_comment_limit', 10)) {
        // block current user
        db_query("UPDATE {users} SET status = 0 WHERE uid = %d", $a1['uid']);
        sess_destroy_uid($a1['uid']);
        // unpublish all user comments
        db_query("UPDATE {comments} SET status = %d WHERE uid = %d", COMMENT_NOT_PUBLISHED, $a1['uid']);
        $res = db_query("SELECT DISTINCT(nid) FROM {comments} WHERE uid = %d", $a1['uid']);
        while ($ob = db_fetch_object($res)) {
          _comment_update_node_statistics($ob->nid);
        }

        watchdog('spam', 'Blocked user %name by reason of mass spam in comments.', array('%name' => check_plain($user->name)));
        cache_clear_all();
      }
    }
  }
}

/**
 * Form builder; Configure the mass spam variables.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function mass_spam_settings() {
  $form = array();

  $form['mass_spam_new_user'] = array(
    '#type' => 'textfield',
    '#title' => t('New user time'),
    '#default_value' => variable_get('mass_spam_new_user', 24),
    '#description' => t('Time in hours that user is new. Default: 24 hours.'),
  );

  $form['mass_spam_comment_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Comment limit'),
    '#default_value' => variable_get('mass_spam_comment_limit', 10),
    '#description' => t('If new user post this number of comment in check period - his account block and comments unpublish. Default: 10 comments.'),
  );

  $form['mass_spam_post_period'] = array(
    '#type' => 'textfield',
    '#title' => t('Check period'),
    '#default_value' => variable_get('mass_spam_post_period', 30),
    '#description' => t('In this period user can\'t post more comment than in comment limit. Default: 30 minutes.'),
  );

  return system_settings_form($form);
}